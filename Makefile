SIM ?= verilator
TOPLEVEL_LANG ?= verilog

PWD=$(shell pwd)

export PYTHONPATH := $(PWD):$(PYTHONPATH)

VERILOG_SOURCES = $(PWD)/mk_fu_cluster.v   
EXTRA_ARGS += --trace 
EXTRA_ARGS += --trace --trace-structs   
   
TOPLEVEL := mk_fu_cluster 
MODULE   := Test_mk_fu_cluster 

 


   
include $(shell cocotb-config --makefiles)/Makefile.sim
  
