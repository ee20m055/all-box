import random
import sys
import cocotb
import logging as log
from cocotb.decorators import coroutine
from cocotb.triggers import Timer, RisingEdge
from cocotb_bus.monitors import BusMonitor
from cocotb_bus.drivers import BusDriver
from cocotb.binary import BinaryValue
from cocotb.regression import TestFactory
from cocotb_bus.scoreboard import Scoreboard
from cocotb.clock import Clock
 
 

 



# A coroutine
async def reset_dut(reset_n, duration_ns):
    reset_n <= 0
    await Timer(duration_ns, units="ns")
    reset_n <= 1
    reset_n._log.debug("Reset complete")

@cocotb.test()
async def parallel_example(dut):
    reset_n = dut.RST_N

    # Execution will block until reset_dut has completed
    await reset_dut(reset_n, 500)
    dut._log.debug("After reset")

    # Run reset_dut concurrently
    reset_thread = cocotb.fork(reset_dut(reset_n, duration_ns=500))

    # This timer will complete before the timer in the concurrently executing "reset_thread"
    await Timer(250, units="ns")
    dut._log.debug("During reset (reset_n = %s)" % reset_n.value)

    # Wait for the other thread to complete
    await reset_thread
    dut._log.debug("After reset")
